﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Application.Helpers
{
    public class ContentInterpreterHelper
    {
        #region Constructors

        public ContentInterpreterHelper()
        {
            _CommentSignes = new List<char>()
            {
                '#',
                '*',
                '/'
            };

            HeaderToFieldMapIndex = new Dictionary<string, int>()
            {
                {"Project",0 },
                {"Description",0 },
                {"Start date",0 },
                {"Category",0 },
                {"Responsible",0 },
                {"Savings amount",0 },
                {"Currency",0 },
                {"Complexity",0 },
            };
        }

        #endregion

        #region Event Handllers

        public event EventHandler<IList<string>> OnDataRead;

        #endregion

        #region Private Fields

        private IList<char> _CommentSignes;
        private bool _HeaderFound = false;

        #endregion

        #region Public Properties

        public IDictionary<string, int> HeaderToFieldMapIndex { get; set; }

        #endregion

        #region Public Methods

        public void ParsLine(string line)
        {
            if (!ValidateLine(line))
                return;

            if (!_HeaderFound)
            {
                if (IsHeader(line))
                {
                    _HeaderFound = true;
                    SetHeaderToPropertyMapIndex(line);
                }
            }
            else
            {
                OnDataRead?.Invoke(this, line.Split('\t').ToList());
            }
        }

        #endregion

        #region Private Methods

        private bool ValidateLine(string line)
        {
            if (string.IsNullOrEmpty(line))
                return false;

            var expPattertn = "(^[" + new string(_CommentSignes.ToArray()) + "])";

            if (Regex.IsMatch(line, expPattertn))
                return false;

            return true;
        }

        private bool IsHeader(string line)
        {
            var expPattertn = string.Empty;

            foreach (var key in HeaderToFieldMapIndex.Keys)
                expPattertn = expPattertn + (string.IsNullOrEmpty(expPattertn) ? string.Empty : "|") + "(^" + key + ")";

            if (Regex.IsMatch(line, expPattertn))
                return true;

            return false;
        }

        private void SetHeaderToPropertyMapIndex(string headerLine)
        {
            var columnHeaders = headerLine.Split('\t');

            for (int i = 0; i < columnHeaders.Count(); i++)
            {
                if (!HeaderToFieldMapIndex.ContainsKey(columnHeaders[i]))
                {
                    throw new Exception($"Unable to match {columnHeaders[i]} to properties.");
                }
                else
                {
                    HeaderToFieldMapIndex[columnHeaders[i]] = i;
                }
            }
        }

        #endregion
    }
}
