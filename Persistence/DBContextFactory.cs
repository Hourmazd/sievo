﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Persistence
{
    public class DBContextFactory : IDesignTimeDbContextFactory<ProjectDbContext>
    {
        public IConfigurationRoot Configuration;

        public DBContextFactory()
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
        }

        public ProjectDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseSqlite(Configuration.GetConnectionString("ProjectsDb"));

            return new ProjectDbContext(optionsBuilder.Options);
        }
    }
}
