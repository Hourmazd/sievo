﻿namespace Infrastructure.Common
{
    public enum Currencies
    {
        NA,
        EUR,
        USD
    }

    public enum Complexities
    {
        Simple, 
        Moderate, 
        Hazardous,
        VeryHigh
    }
}
