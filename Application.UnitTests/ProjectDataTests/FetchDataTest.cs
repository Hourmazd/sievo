﻿using Application.CubeSensors.Queries;
using Application.Projects.Commands;
using Application.UnitTests.Common;
using Domain.Entities;
using MediatR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shouldly;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.UnitTests.ProjectDataTests
{
    [TestClass]
    public class FetchDataTest : CommandTestBase
    {
        [TestMethod]
        public async Task Handle_SeedAndReadData()
        {
            var mediatorMock = new Mock<IMediator>();
            var sut = new UpsertProjects.UpsertProjectHandler(_Context, mediatorMock.Object);

            for (var i = 1; i <= 20; i++)
            {
                var data = new UpsertProjects.UpsertProjectCommad()
                {
                    entity = new ProjectData()
                    {
                        ProjectId = i,
                        Description = "TEST PROJECT " + i,
                        StartDate = DateTime.Now.AddDays(i),
                        Category = "acidophilus",
                        Responsible = "sourcing",
                        SavingsAmount = 0,
                        Currency = Domain.Enumurations.Currencies.EUR,
                        Complexity = Domain.Enumurations.Complexities.Moderate
                    }
                };
                await sut.Handle(data, CancellationToken.None);
            }

            // Act
            var readHandler = new FetchProjects.Handler(_Context);
            var query = new FetchProjects.Query() { ProjectId = 1 };
            var result = await readHandler.Handle(query, CancellationToken.None);

            // Assert
            result.Count().ShouldBe(1, "List should contains 1 elements.");

            query = new FetchProjects.Query() { ProjectId = null };
            result = await readHandler.Handle(query, CancellationToken.None);

            // Assert
            result.Count().ShouldBe(20, "List should contains 20 elements.");
        }

    }
}
