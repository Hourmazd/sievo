﻿using Application.Common.Exceptions;
using Domain.Entities;
using MediatR;
using Persistence;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Projects.Commands
{
    public class DeleteProject
    {
        public class DeleteProjecCommand : IRequest
        {
            public int Id { get; set; }
        }

        public class DeleteProjecHandler : IRequestHandler<DeleteProjecCommand>
        {
            private readonly ProjectDbContext _Context;

            public DeleteProjecHandler(ProjectDbContext context)
            {
                _Context = context;
            }

            public async Task<Unit> Handle(DeleteProjecCommand request, CancellationToken cancellationToken)
            {
                var entity = await _Context.Projects.FindAsync(request.Id);

                if (entity == null)
                    throw new NotFoundException(nameof(ProjectData), request.Id);

                _Context.Remove(entity);

                var success = await _Context.SaveChangesAsync() > 0;
                if (success) return Unit.Value;
                throw new Exception("Error deleting entity.");
            }
        }
    }
}
