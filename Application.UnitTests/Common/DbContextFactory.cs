﻿using Microsoft.EntityFrameworkCore;
using Persistence;
using System;

namespace Application.UnitTests.Common
{
    public class DbContextFactory
    {
        public static ProjectDbContext Create()
        {
            var options = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new ProjectDbContext(options);

            context.Database.EnsureCreated();
            
            return context;
        }

        public static void Destroy(ProjectDbContext context)
        {
            context.Database.EnsureDeleted();

            context.Dispose();
        }
    }
}
