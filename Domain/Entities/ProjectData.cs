﻿using Domain.Enumurations;
using Infrastructure.Common.CustomJsonConverter;
using Newtonsoft.Json;
using System;

namespace Domain.Entities
{
    public class ProjectData
    {
        [JsonIgnore]
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public string Description { get; set; }

        [JsonProperty("Start date")]
        [JsonConverter(typeof(DateToStringConverter))]
        public DateTime StartDate { get; set; }

        public string Category { get; set; }

        public string Responsible { get; set; }

        [JsonProperty("Savings amount")]
        [JsonConverter(typeof(DecimalToStringConverter))]
        public decimal? SavingsAmount { get; set; }

        [JsonConverter(typeof(CurrencyToStringConverter))]
        public Currencies? Currency { get; set; }

        public Complexities Complexity { get; set; }

        public override string ToString()
        {
            return
                ProjectId + "\t" +
                Description + "\t" +
                StartDate.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\t" +
                Category + "\t" +
                Responsible + "\t" +
                (SavingsAmount.HasValue ? SavingsAmount.Value.ToString() : string.Empty) + "\t" +
                (Currency.HasValue ? Currency.Value.ToString() : string.Empty) + "\t" +
                Complexity + "\t";
        }
    }
}
