﻿using Newtonsoft.Json;
using System;

namespace Infrastructure.Common.CustomJsonConverter
{
    public class CurrencyToStringConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Currencies);
        }

        public override bool CanRead => false;

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException("Unnecessary");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((Currencies)value) == Currencies.NA ? string.Empty : ((Currencies)value).ToString());
        }
    }
}
