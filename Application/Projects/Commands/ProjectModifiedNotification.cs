﻿using Infrastructure.Notification;
using Infrastructure.Notification.Notification;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Projects.Commands
{
    public class ProjectModifiedNotification : INotification
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }

        public class SensorDataModifiedHandler : INotificationHandler<ProjectModifiedNotification>
        {
            private readonly NotificationService _notification;

            public SensorDataModifiedHandler(NotificationService notification)
            {
                _notification = notification;
            }

            public async Task Handle(ProjectModifiedNotification notification, CancellationToken cancellationToken)
            {
                await _notification.SendAsync(new NotificationDto());
            }
        }
    }
}
