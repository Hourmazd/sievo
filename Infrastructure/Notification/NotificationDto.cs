﻿namespace Infrastructure.Notification
{
    public class NotificationDto
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
