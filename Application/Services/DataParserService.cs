﻿using Application.Helpers;
using Application.Projects.Commands;
using Persistence;
using System.Collections.Generic;
using MediatR;
using System.Threading.Tasks;
using System.Threading;
using Application.IServices;
using Domain.Entities;
using Application.CubeSensors.Queries;

namespace Application.Services
{
    public class DataParserService : IDataParserService
    {
        #region Constructors

        public DataParserService(ProjectDbContext context, IMediator mediator)
        {
            _Context = context;
            _Mediator = mediator;

            _FileReader = new FileReaderHelper();
            _FileReader.OnLineRead += _FileReader_OnLineRead;

            _ContentInterpreter = new ContentInterpreterHelper();
            _ContentInterpreter.OnDataRead += _ContentInterpreter_OnDataRead;

            _DataInterpreter = new DataInterpreter();
            _DataInterpreter.OnEntityCreated += _DataInterpreter_OnEntityCreated;
        }

        #endregion

        #region Private Fields

        private FileReaderHelper _FileReader;
        private ContentInterpreterHelper _ContentInterpreter;
        private DataInterpreter _DataInterpreter;

        private ProjectDbContext _Context;
        private IMediator _Mediator;

        #endregion
        
        #region Public Methods

        public void ParseFile(string filePath)
        {
            _FileReader.ReadFileContent(filePath);
        }

        public async Task<IList<ProjectData>> FetchProjects(int? projectId, bool sortByStartDate)
        {
            var handler = new FetchProjects.Handler(_Context);
            var command = new FetchProjects.Query() { ProjectId = projectId, SortByStartDate = sortByStartDate };

            return await handler.Handle(command, CancellationToken.None);
        }

        public string GetHeaderRow()
        {
            var result = string.Empty;

            foreach (var key in _ContentInterpreter.HeaderToFieldMapIndex.Keys)
                result = result + key + "\t";

            return result;
        }

        public async Task ClearProjects()
        {
            var handler = new DeleteProject.DeleteProjecHandler(_Context);

            foreach (var entity in _Context.Projects)
            {
                var command = new DeleteProject.DeleteProjecCommand() { Id = entity.Id };
                await handler.Handle(command, CancellationToken.None);
            }
        }

        #endregion

        #region Private Methods

        private void _FileReader_OnLineRead(object sender, string e)
        {
            _ContentInterpreter.ParsLine(e);
        }

        private void _ContentInterpreter_OnDataRead(object sender, IList<string> e)
        {
            _DataInterpreter.ParseData(e, _ContentInterpreter.HeaderToFieldMapIndex);
        }

        private async void _DataInterpreter_OnEntityCreated(object sender, ProjectData e)
        {
            var handler = new UpsertProjects.UpsertProjectHandler(_Context, _Mediator);
            var command = new UpsertProjects.UpsertProjectCommad()
            {
                entity = e
            };

            await handler.Handle(command, CancellationToken.None);
        }

        #endregion
    }
}
