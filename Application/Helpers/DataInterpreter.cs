﻿using Domain.Entities;
using Domain.Enumurations;
using System;
using System.Collections.Generic;

namespace Application.Helpers
{
    public class DataInterpreter
    {
        #region Event Handllers

        public event EventHandler<ProjectData> OnEntityCreated;

        #endregion

        #region Public Methods

        public void ParseData(IList<string> data, IDictionary<string, int> map)
        {
            var result = new ProjectData()
            {
                ProjectId = GetProjectId(data, map),
                Description = GetDescription(data, map),
                StartDate = GetStartDate(data, map),
                Responsible = GetResponsible(data, map),
                Category = GetCategory(data, map),
                SavingsAmount = GetSavingAmount(data, map),
                Currency = GetCurrency(data, map),
                Complexity = GetComplexity(data, map)
            };

            OnEntityCreated?.Invoke(this, result);
        }

        #endregion

        #region Private Methods

        private int GetProjectId(IList<string> data, IDictionary<string, int> map)
        {
            var datastring = data[map["Project"]];

            if (!int.TryParse(datastring, out int id))
            {
                throw new Exception($"Unable to parse '{datastring}' as ProjectId");
            }

            return id;
        }

        private string GetDescription(IList<string> data, IDictionary<string, int> map)
        {
            return data[map["Description"]];
        }

        private DateTime GetStartDate(IList<string> data, IDictionary<string, int> map)
        {
            var datastring = data[map["Start date"]];

            if (!DateTime.TryParse(datastring, out DateTime date))
            {
                throw new Exception($"Unable to parse '{datastring}' as Start date");
            }

            return date;
        }

        private string GetResponsible(IList<string> data, IDictionary<string, int> map)
        {
            return data[map["Responsible"]];
        }

        private string GetCategory(IList<string> data, IDictionary<string, int> map)
        {
            return data[map["Category"]];
        }

        private decimal? GetSavingAmount(IList<string> data, IDictionary<string, int> map)
        {
            var datastring = data[map["Savings amount"]];

            if (datastring == "NULL")
                return null;

            if (!decimal.TryParse(datastring, out decimal amount))
            {
                throw new Exception($"Unable to parse '{datastring}' as saving amount.");
            }

            return amount;
        }

        private Currencies? GetCurrency(IList<string> data, IDictionary<string, int> map)
        {
            var datastring = data[map["Currency"]];

            if (datastring == "NULL")
                return null;

            if (!Enum.TryParse<Currencies>(datastring, true, out Currencies currency))
            {
                throw new Exception($"Unable to parse '{datastring}' as currency.");
            }

            return currency;
        }

        private Complexities GetComplexity(IList<string> data, IDictionary<string, int> map)
        {
            var datastring = data[map["Complexity"]];

            if (!Enum.TryParse<Complexities>(datastring, true, out Complexities complexity))
            {
                throw new Exception($"Unable to parse '{datastring}' as complexity.");
            }

            return complexity;
        }

        #endregion
    }
}
