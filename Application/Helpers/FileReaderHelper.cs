﻿using System;
using System.IO;
using System.Text;

namespace Application.Helpers
{
    public class FileReaderHelper
    {
        #region Event Handllers

        public event EventHandler<string> OnLineRead;

        #endregion

        #region Public Methods

        public void ReadFileContent(string filePath)
        {
            using var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            using var sr = new StreamReader(fs, Encoding.UTF8);

            var line = string.Empty;

            while ((line = sr.ReadLine()) != null)
            {
                if (OnLineRead != null)
                    OnLineRead.Invoke(this, line);
            }
        }

        #endregion
    }
}
