﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace Persistence
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext([NotNullAttribute] DbContextOptions<ProjectDbContext> options)
            : base(options)
        {
        }

        public DbSet<ProjectData> Projects { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ProjectData>(entity =>
            {
                entity.HasKey(e => e.Id)
                .HasName("PK_Id");

                entity.HasIndex(e => e.ProjectId)
                    .HasName("IX_Projects_ProjectId"); 

                entity.HasIndex(e => e.StartDate)
                    .HasName("IX_Projects_StartDate");
            });
        }
    }
}
