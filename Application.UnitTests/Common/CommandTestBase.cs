﻿using Persistence;
using System;

namespace Application.UnitTests.Common
{
    public class CommandTestBase : IDisposable
    {
        protected readonly ProjectDbContext _Context;

        public CommandTestBase()
        {
            _Context = DbContextFactory.Create();
        }

        public void Dispose()
        {
            DbContextFactory.Destroy(_Context);
        }
    }
}
