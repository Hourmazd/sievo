﻿using Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.IServices
{
    public interface IDataParserService
    {
        void ParseFile(string filePath);

        Task ClearProjects();

        Task<IList<ProjectData>> FetchProjects(int? projectId, bool sortByStartDate);

        string GetHeaderRow();
    }
}
