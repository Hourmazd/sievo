## Project Spesifications
Arcitecture: Clean Arcitecture (Monolith)
Platform: Cross Platform

## Technologies
* .NET Core 3
* ASP.NET Core 3
* Entity Framework Core 3

## Tools
* MediatR
* Linq

## Prerequisites
You will need the following tools:

* [Visual Studio Code or Visual Studio 2019](https://visualstudio.microsoft.com/vs/)
* [.NET Core SDK 3](https://dotnet.microsoft.com/download/dotnet-core/3.0)

## Run
Follow these steps to get your development environment set up:

  1. Clone the repository
  2. At the root solution directory, restore required packages by running:
     dotnet restore
     
  3. Next, build the solution by running:
     dotnet build
     
  4. Relocate to the `\ConsoleAPI` directory
	 cd ConsoleAPI

  5. Next, within the `\ConsoleAPI` directory, launch the back end by:
     dotnet run -File <sourceFilePath> [-SortByStartDate] [-Project <projectId>]
     	 
	 
## Some Descriptions
* For this solution it is not needed to create database and develop Migration Scripts, but I did it becuase I wanted to develop Clean Arcitecture and (DDD) from the begining
* I did not consider the performance as has mentioned in the description
* By runing the project, one local Sqlite db will be creater and all data will be saved in db, then data will fetch from the db
* I know there are so many simpler solutions for this exsercise, I did it more complex