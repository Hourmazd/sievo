﻿using Application.UnitTests.Common;
using System;
using System.Linq;
using MediatR;
using Moq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Application.Projects.Commands;
using Shouldly;
using Domain.Entities;

namespace Application.UnitTests.ProjectDataTests
{
    [TestClass]
    public class DataManipulationTest : CommandTestBase
    {
        [TestMethod]
        public void Handle_Add_New_ProjectData()
        {
            // Arrange
            var mediatorMock = new Mock<IMediator>();
            var sut = new UpsertProjects.UpsertProjectHandler(_Context, mediatorMock.Object);

            var data = new UpsertProjects.UpsertProjectCommad()
            {
                entity = new ProjectData()
                {
                    ProjectId = 1,
                    Description = "TEST PROJECT 01",
                    StartDate = DateTime.Now,
                    Category = "acidophilus",
                    Responsible = "sourcing",
                    SavingsAmount = 0,
                    Currency = Domain.Enumurations.Currencies.EUR,
                    Complexity = Domain.Enumurations.Complexities.Moderate
                }
            };

            // Act
            var result = sut.Handle(data, CancellationToken.None);

            // Assert
            mediatorMock.Verify(m => m.Publish(It.Is<ProjectModifiedNotification>(cc => cc.ProjectId == 1), It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public void Handle_Delete_ProjectData()
        {
            Handle_Add_New_ProjectData();

            var entity1 = _Context.Projects.FirstOrDefault();
            entity1.ShouldNotBeNull("project entity should not be null.");

            // Arrange
            var sut = new DeleteProject.DeleteProjecHandler(_Context);

            var data = new DeleteProject.DeleteProjecCommand()
            {
                Id = entity1.Id
            };

            // Act
            var result = sut.Handle(data, CancellationToken.None);

            // Assert
            var entity2 = _Context.Projects.FirstOrDefault(e => e.ProjectId == entity1.ProjectId);
            entity2.ShouldBeNull("project entity should be null.");
        }
    }
}
