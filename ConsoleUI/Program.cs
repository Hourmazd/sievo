﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Application.IServices;
using Application.Projects.Commands;
using Application.Services;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Persistence;
using Infrastructure.Notification.Notification;
using System.Collections.Generic;

namespace ConsoleUI
{
    class Program
    {
        private static IConfigurationRoot Configuration;
        private static ServiceProvider ServiceProvider;

        static async Task Main(string[] args)
        {
            InitProgram();

            var service = ServiceProvider.GetService<IDataParserService>();

            try
            {
                var commands = GetCommandParameters(args);

                await service.ClearProjects();

                service.ParseFile(commands.Path);

                var projects = await service.FetchProjects(commands.ProjectId, commands.SortByStartDate);

                Console.WriteLine(service.GetHeaderRow());

                foreach (var project in projects)
                    Console.WriteLine(project.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(0);
            }
        }

        private static void InitProgram()
        {
            Console.OutputEncoding = Encoding.UTF8;

            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            ServiceProvider = new ServiceCollection()
                .AddLogging()
                .AddMediatR(typeof(UpsertProjects.UpsertProjectHandler).Assembly)
                .AddScoped<IDataParserService, DataParserService>()
                .AddScoped<NotificationService>()
                .AddDbContext<ProjectDbContext>(opt =>
                    {
                        opt.UseSqlite(Configuration.GetConnectionString("ProjectsDb"));
                    })
                .BuildServiceProvider();

            try
            {
                var context = ServiceProvider.GetRequiredService<ProjectDbContext>();
                context.Database.Migrate();
            }
            catch (Exception ex)
            {
                var logger = ServiceProvider.GetRequiredService<ILogger<Program>>();
                logger.LogError(ex, "Error in migrating DB.");
            }
        }

        private static ExecutionParameters GetCommandParameters(string[] args)
        {
            var result = new ExecutionParameters();
            var arguments = new List<string>(args);

            if (!arguments.Contains("-File"))
                throw new Exception("Please provide path of the source file in the -File argument.");

            if (arguments.Count < arguments.IndexOf("-File") + 2)
                throw new Exception("Please provide path of the source file after the -File argument.");

            var path = arguments[arguments.IndexOf("-File") + 1];

            var directory = Path.GetDirectoryName(path);
            if (string.IsNullOrEmpty(directory) || !Directory.Exists(directory))
                Console.WriteLine("Input value for directory is not valid. Please try again.");
            else
                result.Path = path;

            if (arguments.Contains("-Project"))
            {
                if (arguments.Count < arguments.IndexOf("-Project") + 2)
                    throw new Exception("Please provide project id after the -Project argument.");

                var projectStr = arguments[arguments.IndexOf("-Project") + 1];

                if (!int.TryParse(projectStr, out int projectId))
                {
                    throw new Exception("Unable to parse ProjectId. Try to use an integer.");
                }

                result.ProjectId = projectId;
            }

            if (arguments.Contains("-SortByStartDate"))
            {
                result.SortByStartDate = true;
            }

            return result;
        }

        internal struct ExecutionParameters
        {
            public string Path;
            public int? ProjectId;
            public bool SortByStartDate;
        }
    }
}
