﻿using MediatR;
using Persistence;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Application.CubeSensors.Queries
{
    public class FetchProjects
    {
        public class Query : IRequest<IList<ProjectData>>
        {
            public int? ProjectId { get; set; }
            public bool SortByStartDate { get; set; }
        }

        public class Handler : IRequestHandler<Query, IList<ProjectData>>
        {
            private readonly ProjectDbContext _Context;

            public Handler(ProjectDbContext context)
            {
                _Context = context;
            }

            public async Task<IList<ProjectData>> Handle(Query request, CancellationToken cancellationToken)
            {
                var result = _Context.Projects.Where(e => request.ProjectId == null || e.ProjectId == request.ProjectId);

                if (request.SortByStartDate)
                    result = result.OrderBy(e => e.StartDate);

                return await result.ToListAsync();
            }
        }
    }
}
