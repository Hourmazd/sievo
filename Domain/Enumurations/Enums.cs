﻿namespace Domain.Enumurations
{
    public enum Currencies
    {
        EUR,
        USD
    }

    public enum Complexities
    {
        Simple, 
        Moderate, 
        Hazardous,
        VeryHigh
    }
}
