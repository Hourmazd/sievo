﻿using Domain.Entities;
using MediatR;
using Persistence;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Projects.Commands
{
    public class UpsertProjects
    {
        public class UpsertProjectCommad : IRequest
        {
            public ProjectData entity { get; set; }
        }

        public class UpsertProjectHandler : IRequestHandler<UpsertProjectCommad>
        {
            private readonly ProjectDbContext _Context;
            private readonly IMediator _Mediator;

            public UpsertProjectHandler(ProjectDbContext context, IMediator mediator)
            {
                _Context = context;
                _Mediator = mediator;
            }

            public async Task<Unit> Handle(UpsertProjectCommad request, CancellationToken cancellationToken)
            {
                var entity = _Context.Projects.FirstOrDefault(e => e.Id == request.entity.Id);

                if (entity == null)
                {
                    entity = new ProjectData();
                    _Context.Projects.Add(entity);
                }

                entity.ProjectId = request.entity.ProjectId;
                entity.Description = request.entity.Description;
                entity.StartDate = request.entity.StartDate;
                entity.Category = request.entity.Category;
                entity.Responsible = request.entity.Responsible;
                entity.SavingsAmount = request.entity.SavingsAmount;
                entity.Currency = request.entity.Currency;
                entity.Complexity = request.entity.Complexity;

                var success = await _Context.SaveChangesAsync() > 0;

                await _Mediator.Publish(new ProjectModifiedNotification { ProjectId = entity.ProjectId }, cancellationToken);

                if (success) return Unit.Value;
                throw new Exception("Error in saving changes.");
            }
        }
    }
}
